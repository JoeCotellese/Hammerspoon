# Hammerspoon
My Hammerspoon config files

## Installation

clone this project into your local git repo. I use ~/git/dotfiles/Hammerspoon
Create a symlink it to ~/.hammerspoon `ln -s ~/git/Hammerspoon ./hammerspoon

## What it does

My Hammerspoon setup does a number of things

* Sets up hot keys to allow me to resize and throw my windows around between screens
* handles properly stopping and unmounting my backup drive when I switch to battery power
* monitors the Media mount on my NAS and remounts it if it disconnects
* turn off / on my Wifi when I connect to ethernet via my USB Hub

Most of this was cobbled together by finding other Hammerspoon config files
over the years. I try to reference the git repo of the original source


