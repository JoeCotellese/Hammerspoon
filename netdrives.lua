-- keep the media drive mounted

local mountEvents = {
    [hs.fs.volume.didMount] = 'mounted',
    [hs.fs.volume.didUnmount] = 'unmounted',
    [hs.fs.volume.willUnmount] = 'willunmount'
}
function handleVolumeEvent(type, details)
    local logger = hs.logger.new('volumeWatcher', 5)
  
    local action = mountEvents[type]
    local volume = details['path']
    local sentence = action .. ' - ' .. details['path'] 
    logger:d(sentence)
  
    if action == 'unmounted' and volume == '/Volumes/Media' then
        hs.applescript.applescript([[
            tell application "Finder"
                try
                    mount volume "smb://jcotellese@BigNas._smb._tcp.local/Media"
                end try
            end tell
        ]])
    end
    
  end
  -- myVolumeWatcher = hs.fs.volume.new(handleVolumeEvent):start()