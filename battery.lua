--
-- Create a watcher that runs a function based on changes to
-- battery state.
--

local battery = {
  source = hs.battery.powerSource()
}

local log = hs.logger.new('battery',info)
function changePowerSource()
  local CurrentPowerSource = hs.battery.powerSource()
  if battery.source ~= CurrentPowerSource then
    log.i("detect change")
    battery.source = CurrentPowerSource
    log.i(battery.source)
    if battery.source == "Battery Power" then
      -- do some cleanup when the battery is unplugged
      log.i("attempting to stop TM")
      shutdownTimeMachine()
    end
  end
end

hs.battery.watcher.new(changePowerSource):start()

function shutdownTimeMachine()
  hs.alert.show("Stopping Time Machine")
  hs.execute("tmutil stopbackup")
  hs.execute("diskutil unmount '/Volumes/Backup_joec'")
end
