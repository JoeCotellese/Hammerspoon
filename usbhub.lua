--
-- Create a watcher that runs a function based on plugging in hub
-- 
--
laptop_hub = 0x0813
usbWatcher = nil
function usbDeviceCallback(data)
    if (data["productID"] == laptop_hub) then
        if (data["eventType"] == "added") then
            connectUSBHub()
        elseif (data["eventType"] == "removed") then
            disconnectUSBHub()
        end
    end
end

function connectUSBHub()
    local log = hs.logger.new('usb',info)
    log.i ("connect Hub")
    -- hs.wifi.setPower(false)
end

function disconnectUSBHub()
    local log = hs.logger.new('usb',info)
    log.i ("disconnect Hub")
    -- hs.wifi.setPower(true)
end

-- usbWatcher = hs.usb.watcher.new(usbDeviceCallback)
-- usbWatcher:start()