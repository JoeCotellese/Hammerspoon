-- Configuration File for hammerspoon
-- A lot of this was swiped from wincent on GitHub
-- https://github.com/wincent/wincent/blob/master/roles/dotfiles/files/.hammerspoon/init.lua

hs.logger.defaultLogLevel = "debug"
local log = hs.logger.new('mymodule','debug')
hyper = {"ctrl", "alt", "cmd"}

hs.loadSpoon("SpoonInstall")
spoon.SpoonInstall.use_syncinstall = true
Install=spoon.SpoonInstall

-- Load External Config Files
-- require ('grid')
require('battery')
require('netdrives')
--require('ejectdisks')
require('apps')

myGrid = { w = 6, h = 4 }
Install:andUse("WindowGrid",
               {
                 config = { gridGeometries =
                              { { myGrid.w .."x" .. myGrid.h } } },
                 hotkeys = {show_grid = {hyper, "g"}},
                 start = true
               }
)
Install:andUse("WindowScreenLeftAndRight",
               {
                 config = {
                   animationDuration = 0
                 },
                 hotkeys = 'default',
--                 loglevel = 'debug'
               }
)

-- Make ejecting disks better
Install:andUse("EjectMenu", {
  config = {
    eject_on_lid_close = false,
    eject_on_sleep = false,
     show_in_menubar = true,
     notify = true,
  },
  hotkeys = { ejectAll = { hyper, "=" } },    
  start = true,
--                 loglevel = 'debug'
})

Install:andUse("WindowHalfsAndThirds",
               {
                 config = {
                   use_frame_correctness = false
                 },
                 hotkeys = {
                  left_half    = { {"ctrl",        "cmd"}, "j" },
                  right_half   = { {"ctrl",        "cmd"}, "l" },
                  top_half     = { {"ctrl",        "cmd"}, "i" },
                  bottom_half  = { {"ctrl",        "cmd"}, "k" },
                  third_left   = { {"ctrl", "alt"       }, "j" },
                  third_right  = { {"ctrl", "alt"       }, "l" },
                  third_up     = { {"ctrl", "alt"       }, "i" },
                  third_down   = { {"ctrl", "alt"       }, "k" },
                  top_left     = { {"ctrl",        "cmd"}, "7" },
                  top_right    = { {"ctrl",        "cmd"}, "8" },
                  bottom_left  = { {"ctrl",        "cmd"}, "9" },
                  bottom_right = { {"ctrl",        "cmd"}, "0" },
                  max_toggle   = { {"ctrl", "alt", "cmd"}, "Home" },
                  -- max          = { {"ctrl", "alt", "cmd"}, "Home" },
                  undo         = { {        "alt", "cmd"}, "z" },
                  center       = { {        "alt", "cmd"}, "c" },
                  larger       = { {        "alt", "cmd", "shift"}, "Right" },
                  smaller      = { {        "alt", "cmd", "shift"}, "Left" },
                  },
                 --loglevel = 'debug'
               }
)


-- Get around paste blockers
hs.hotkey.bind({"cmd", "alt"}, "v", function() 
  hs.eventtap.keyStrokes(hs.pasteboard.getContents()) 
end)

hs.pathwatcher.new(os.getenv('HOME') .. '/.hammerspoon/', hs.reload):start()
